This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Things to improve upon:
-----------------------
-Obviously needs a persistent database. This app is really only for demo purposes at the moment.
Currently everything is stored in that state and disappears on page refresh
-Design a better means for searching IMdb. The app currently uses the first search result that
 matches the user-input
-Checking for correct data types, such as integer value for rating. this could also be avoided by
 using a drop-down or some other component which disallows incorrect input
-Check for duplicate movies on a list. I do check if the movie is already added in general, but
not if its already on the list.
-Additional animations and feedback for the user to understand what's going on
-Better error handling
-Movies should link to their IMdb page
-Should round the average ratings to 1 to 2 decimal points
-Not entirely sure if this is compatible with all browsers. I know for certain it works correctly on
Chrome 62.

