import React from "react";
import ReactTable from "react-table";
import "./MovieLists.css";

class MovieListsTable extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.createRow = this.createRow.bind(this);
  }

  createRow(item, key) {
    var movieCount = item.movies.length;
    var rating = this.calculateListRating(item.movies);

    return {
      key: key,
      name: item.name,
      rating: rating,
      movies: movieCount
    };
  }

  // calculate average movie ratings in list
  calculateListRating(movies) {
    var ratings = movies.map(function(movie) {
      return movie.rating;
    });

    ratings = ratings.filter(function(rating) {
      return rating !== null;
    });

    ratings = ratings.map(function(rating) {
      return parseInt(rating, 10);
    });

    if (ratings.length !== 0) {
      var sum = ratings.reduce((previous, current) => (current += previous));
      var average = sum / ratings.length;
    } else average = "-";

    return average;
  }

  handleClick(state, rowInfo, column, instance) {
    return {
      onClick: (e, handleOriginal) => {
        this.props.setActive("MOVIES", rowInfo.row.key);
        if (handleOriginal) {
          handleOriginal();
        }
      }
    };
  }

  render() {
    var rowEntries = this.props.entries;
    var rowItems = [];
    for (var entry in rowEntries) {
      rowItems.push(this.createRow(rowEntries[entry], entry));
    }

    const columns = [
      {
        accessor: "key",
        show: false
      },
      {
        Header: "Name",
        accessor: "name" // String-based value accessors!
      },
      {
        Header: "Rating",
        accessor: "rating"
      },
      {
        Header: "Movies",
        accessor: "movies" // Required because our accessor is not a string
      }
    ];

    return (
      <ReactTable
        defaultPageSize={10}
        data={rowItems}
        columns={columns}
        getTrProps={this.handleClick.bind(this)}
      />
    );
  }
}

export default MovieListsTable;
