import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import MovieLists from "./MovieLists.js";
import "font-awesome/css/font-awesome.min.css";

var destination = document.querySelector("#container");

ReactDOM.render(
  <div>
    <MovieLists />
  </div>,
  destination
);
