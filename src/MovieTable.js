import React from "react";
import ReactTable from "react-table";
import "./MovieLists.css";

class MovieTable extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.createRow = this.createRow.bind(this);
  }

  createRow(item) {
    return {
      id: item.id,
      poster: item.poster,
      name: item.name,
      genre: item.genre,
      rating: item.rating,
      year: item.year
    };
  }

  deleteRow(key) {
    this.props.deleteMovieFromList(key);
  }

  //TODO: check if integer
  rateMovie(rating, id, e) {
    if (rating !== "") {
      this.props.rateMovie(id, rating);
    }

    e.preventDefault();
  }

  render() {
    var rowEntries = this.props.entries;
    var rowItems = [];
    for (var entry in rowEntries) {
      rowItems.push(this.createRow(rowEntries[entry]));
    }

    const columns = [
      {
        accessor: "id",
        show: false
      },
      {
        className: "poster-image",
        Header: "Poster",
        accessor: "poster",
        Cell: props => <img src={props.value} alt="" />,
        sortable: false
      },
      {
        Header: "Name",
        accessor: "name",
        width: 200
      },
      {
        Header: "Year",
        accessor: "year"
      },
      {
        Header: "Genre",
        accessor: "genre",
        width: 200
      },
      {
        Header: "Rating",
        accessor: "rating"
      }
    ];

    return (
      <ReactTable
        defaultPageSize={10}
        minRows={5}
        data={rowItems}
        columns={columns}
        SubComponent={row => {
          return (
            <div className="expand-opts">
              <form
                className="opt"
                onSubmit={e =>
                  this.rateMovie(this._inputElement.value, row.original.id, e)
                }
              >
                <label>
                  {" "}
                  Rating
                  <input
                    ref={a => (this._inputElement = a)}
                    placeholder="1-100"
                  />
                </label>
              </form>
              <i
                className="fa fa-trash-o"
                aria-hidden="true"
                onClick={() => this.props.deleteMovieFromList(row.original.id)}
              />
            </div>
          );
        }}
      />
    );
  }
}

export default MovieTable;
