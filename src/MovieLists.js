import React from "react";
import MovieListsTable from "./MovieListsTable";
import MovieTable from "./MovieTable";

class MovieLists extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      listItems: {},
      movieItems: {},
      active: "LISTS",
      activeList: null,
      errorMessage: ""
    };

    this.addListItem = this.addListItem.bind(this);
    this.addMovieItem = this.addMovieItem.bind(this);
    this.setActive = this.setActive.bind(this);
  }

  addListItem(e) {
    var listObject = this.state.listItems;

    if (this._inputElement.value !== "") {
      listObject[Date.now()] = {
        name: this._inputElement.value,
        movies: []
      };

      this.setState({
        listItems: listObject
      });

      this._inputElement.value = "";
    }

    e.preventDefault();
  }

  addMovieItem(e) {
    var movieObject = this.state.movieItems;
    var searchValue = this._inputElement.value;

    if (searchValue !== "") {
      while (searchValue.indexOf(" ") !== -1) {
        searchValue = searchValue.replace(" ", "+");
      }

      var url =
        "http://www.omdbapi.com/?apikey=c85d5c7f&t=%22" +
        searchValue +
        "%22&type=movie";

      fetch(url)
        .then(function(response) {
          if (response.ok && response.status === 200) {
            return response.json();
          } else {
            var error = "Something went wrong with your fetch.";
            console.log(error);
            throw error;
          }
        })
        .then(obj => {
          if (obj.Response !== "False") {
            if (!(obj.imdbID in movieObject)) {
              movieObject[obj.imdbID] = {
                name: obj.Title,
                year: obj.Year,
                genre: obj.Genre,
                poster: obj.Poster,
                rating: null,
                id: obj.imdbID
              };

              this.setState({
                movieItems: movieObject
              });
            }

            this.addMovieToList(this.state.activeList, movieObject[obj.imdbID]);
          } else throw obj.Error;
        })
        .catch(error => {
          console.log(error);
          this.setState({
            errorMessage: error
          });
        });

      this._inputElement.value = "";
    }

    e.preventDefault();
  }

  //TODO: use active list key?
  addMovieToList(listKey, movie) {
    var listObject = this.state.listItems;
    listObject[listKey].movies.push(movie);

    this.setState({ listItems: listObject });
  }

  deleteMovieFromList(id) {
    var listObject = this.state.listItems;
    listObject[this.state.activeList].movies = listObject[
      this.state.activeList
    ].movies.filter(function(movie) {
      return movie.id !== id;
    });

    this.setState({ listItems: listObject });
  }

  rateMovie(id, rating) {
    var movieObject = this.state.movieItems;
    movieObject[id].rating = rating;

    this.setState({ movieItems: movieObject });
  }

  setActive(a, listKey) {
    this.setState({
      active: a,
      activeList: listKey
    });
  }

  goBack() {
    this.setState({ active: "LISTS" });
  }

  render() {
    var active = this.state.active;

    if (active === "MOVIES") {
      var listObject = this.state.listItems;
      var movies = listObject[this.state.activeList].movies;
    }

    return (
      <div>
        {active === "LISTS" ? (
          <div className="movieListsMain">
            <h1 className="title"> My Movie Lists</h1>

            <form onSubmit={this.addListItem}>
              <div className="add-form">
                <input
                  ref={a => (this._inputElement = a)}
                  placeholder="Enter New List Title"
                />

                <button type="submit">
                  <i className="fa fa-plus" aria-hidden="true" />
                </button>
              </div>
            </form>
            <MovieListsTable
              entries={this.state.listItems}
              setActive={this.setActive.bind(this)}
            />
          </div>
        ) : active === "MOVIES" ? (
          <div className="listView">
            <div>
              <button className="back-button" onClick={this.goBack.bind(this)}>
                <i className="fa fa-arrow-left" aria-hidden="true" />
              </button>
              <h1 className="title">
                {this.state.listItems[this.state.activeList].name}
              </h1>
              <form onSubmit={this.addMovieItem}>
                <div className="add-form">
                  <input
                    ref={a => (this._inputElement = a)}
                    placeholder="Enter New Movie Title"
                  />
                  <button type="submit">
                    <i className="fa fa-plus" aria-hidden="true" />
                  </button>
                </div>
              </form>
              <p className="error">{this.state.errorMessage}</p>
            </div>
            <MovieTable
              entries={movies}
              addMovieToList={this.addMovieToList.bind(this)}
              deleteMovieFromList={this.deleteMovieFromList.bind(this)}
              rateMovie={this.rateMovie.bind(this)}
            />
          </div>
        ) : null}
      </div>
    );
  }
}

export default MovieLists;
